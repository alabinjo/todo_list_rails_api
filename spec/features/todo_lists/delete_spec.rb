require "spec_helper"

describe "deleting lists" do 
  todo_list = TodoList.create(title: "Groceries", description: "hello list")
  it "should delete a list" do 
    #visit todo_list index 
    visit "/todo_lists"
    
    #click on destroy with a certain id
    within "#todo_list_#{todo_list.id}" do
      click_link "Destroy"
    end 
    #expect page to have pop up
    expect(page).to_not have_content(todo_list.title)
    expect(page).to_not have_content(todo_list.description)
    expect(TodoList.count).to eq(0)
  end 
  
end 