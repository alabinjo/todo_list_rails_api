class ItemsController < ApplicationController
 
  #before_action :set_item, only: [:index, :new, :create, :edit, :update]
 
  
  def index
    #binding.pry
    @todo_list = TodoList.find(params[:todo_list_id])
  end
  
  def show 
    @todo_list = TodoList.find(params[:todo_list_id])
    @item = @todo_list.items.find(params[:id])
  end 
  
  def new 
    @todo_list = TodoList.find(params[:todo_list_id])
    @item = @todo_list.items.build
  end 
  
  def create 
    @todo_list = TodoList.find(params[:todo_list_id])
    @item = @todo_list.items.build(item_params)
    if @item.save 
      flash[:success] = "Your item has been created succesfully"
      redirect_to todo_list_items_path(@todo_list)
    else 
      flash.now[:error] = "Something went wrong with your form"
      render :new
    end 
  end 
  
  def edit 
    @todo_list = TodoList.find(params[:todo_list_id])
    @item = @todo_list.items.find(params[:id])
  end 
  
  def update
    @todo_list = TodoList.find(params[:todo_list_id])
    @item = @todo_list.items.find(params[:id])
    if @item.update(item_params)
      flash[:success] = "Your item has been udpated"
      redirect_to todo_list_items_path
    else 
      flash.now[:error] = "Your item failed to update"
      render :edit 
    end 
  end 
  
  def destroy
    @todo_list = TodoList.find(params[:todo_list_id])
    @item = @todo_list.items.find(params[:id])
    @item.destroy 
    flash[:success] = "Your item has been succesfully deleted"
    redirect_to todo_list_items_path(@todo_list)
  end 
  
  private 
  
    def item_params
      params.require(:item).permit(:todo_list_id, :content, :status, :id)
    end
    
    #def set_item
      # @todo_list = TodoList.find(params[:todo_list_id])
       #@item = @todo_list.items.find(params[:id])
    #end 
    

end
