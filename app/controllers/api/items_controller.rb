class Api::ItemsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_filter :find_todo_list
  
  def create
    @item = @todo_list.items.new(item_params)
    if @item.save 
      render json: {
        status: 200,
        messeage: "Sucessfully created item.",
        todo_list: @item
      }.to_json 
        
    else
      render json: { status: 422, message: "Error creating item", todo_list: @item }.to_json
    end
  end


  private 
   
  def item_params
     params.require(:todo_list).permit(:title, :description)
  end 
   
  def find_todo_list 
    @todo_list = TodoList.find(params[:todo_list_id])
  end 

end 