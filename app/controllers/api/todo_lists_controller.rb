class Api::TodoListsController < ApplicationController 
  
  skip_before_filter :verify_authenticity_token
  
  def index
    @todo_lists = TodoList.all
    render json: @todo_lists
  end 
  
  def shows
    @todo_list = TodoList.find(params[:id])
    respond_to do |format|
      format.json { render :json => @todo_list }
    end 
  end 
  
  def new 
    @todo_list = TodoList.new 
  end 
  
  def create
    @todo_list = TodoList.new(todo_list_params)
    if @todo_list.save 
      render json: {
        status: 200,
        messeage: "Sucessfully created Todo list.",
        todo_list: @todo_list
      }.to_json 
        
    else
      render json: { status: 422, message: "Error creating Todo list", todo_list: @todo_list }.to_json
    end
  end 
  
  def edit 
    @todo_list = Todolist.find(params[:id])
  end 
  
  def update 
    @todo_list = TodoList.find
    if @todo_list.update(todo_list_params)
      render json: { status: 200, message: "Sucessfully updated Todo list", todo_list: @todo_list }.to_json
    else 
      render json: { status: 422, message: "Error updating Todo list", todo_list: @todo_list }.to_json
    end 
  end 
  
  def destroy 
    @todo_list = TodoList.find(params[:id])
    @todo_list.destroy 
    render json: {
        status: 200,
        messeage: "Sucessfully destroyed Todo list.",
        todo_list: @todo_list
      }.to_json
  end 
   
   private 
   
   def todo_list_params
     params.require(:todo_list).permit(:title, :description)
   end 
end 